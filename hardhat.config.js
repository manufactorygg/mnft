require('@nomiclabs/hardhat-waffle')
require('@nomiclabs/hardhat-ethers')
require('@nomiclabs/hardhat-etherscan')
require('dotenv').config()

const BSC_MAINNET_RPC_URL = process.env.BSC_MAINNET_RPC_URL || 'https://bsc-mainnet.gateway.pokt.network/v1/<APP_ID>'
const RINKEBY_RPC_URL = process.env.RINKEBY_RPC_URL || 'https://eth-rinkeby.gateway.pokt.network/v1/<APP_ID>'
const POLYGON_MAINNET_RPC_URL = process.env.POLYGON_MAINNET_RPC_URL || 'https://poly-mainnet.gateway.pokt.network/v1/<APP_ID>'
const ETHERSCAN_API_KEY = process.env.ETHERSCAN_API_KEY || 'Your etherscan API key'
const PRIVATE_KEY = process.env.PRIVATE_KEY || 'your private key'

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  defaultNetwork: 'binance',
  networks: {
    hardhat: {},
    binance: {
      url: BSC_MAINNET_RPC_URL,
      accounts: [PRIVATE_KEY],
      saveDeployments: true,
    },
    rinkeby: {
      url: RINKEBY_RPC_URL,
      accounts: [PRIVATE_KEY],
      saveDeployments: true,
    },
    polygon: {
      url: POLYGON_MAINNET_RPC_URL,
      accounts: [PRIVATE_KEY],
      saveDeployments: true,
    },
  },
  etherscan: {
    // Your API key for Etherscan
    // Obtain one at https://etherscan.io/
    apiKey: ETHERSCAN_API_KEY,
  },
  solidity: {
    version: '0.8.2',
    settings: {
      optimizer: {
        enabled: true,
        runs: 99999,
      },
    },
  },
}
